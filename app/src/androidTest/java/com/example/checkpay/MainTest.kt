package com.example.checkpay

import android.content.pm.ActivityInfo
import android.provider.ContactsContract.Directory.PACKAGE_NAME
import android.text.method.Touch.getInitialScrollY
import android.text.method.Touch.scrollTo
import android.widget.Toast
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.ComponentNameMatchers.hasShortClassName
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.hamcrest.CoreMatchers.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainTest{
    @Rule @JvmField
    var activityTestRule = ActivityTestRule(activity_home::class.java)

    //Testing untuk mengecek apakah splashscreen muncul
    @Test
    fun test_isSplashScreenInView(){
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.main)).check(matches(isDisplayed()))
    }

    //Testing untuk mengecek apakah semua komponen pada activity home muncul
    @Test
    fun test_VisibilityHome() {
        onView(withId(R.id.logo))
                .check(matches(isDisplayed()))
        onView(withId(R.id.hiuser))
                .check(matches(isDisplayed()))
        onView(withId(R.id.nis))
                .check(matches(isDisplayed()))
        onView(withId(R.id.nama))
                .check(matches(isDisplayed()))
        onView(withId(R.id.kelas))
                .check(matches(isDisplayed()))
        onView(withId(R.id.cek))
                .check(matches(isDisplayed()))
        onView(withId(R.id.cekuangsekolah))
                .check(matches(isDisplayed()))
    }

    //Testing untuk mengecek fungsi button cek
    @Test
    fun clickCekButton(){
        onView(withId(R.id.nis)).perform(ViewActions.typeText("3868"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        onView(withId(R.id.nama)).check(matches(withText("Marco")))
        onView(withId(R.id.kelas)).check(matches(withText("A")))
        onView(withId(R.id.nis)).perform(ViewActions.clearText())
        onView(withId(R.id.nis)).perform(ViewActions.typeText("3789"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        onView(withId(R.id.nama)).check(matches(withText("Neveline")))
        onView(withId(R.id.kelas)).check(matches(withText("B")))
        onView(withId(R.id.nis)).perform(ViewActions.clearText())
        onView(withId(R.id.nis)).perform(ViewActions.typeText("3685"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        onView(withId(R.id.nama)).check(matches(withText("Anthony")))
        onView(withId(R.id.kelas)).check(matches(withText("C")))
        onView(withId(R.id.cekuangsekolah)).perform(click())
    }

    //Testing untuk mengecek fungsi button cek uang sekolah
    @Test
    fun clickCekUangSklhButton(){
        onView(withId(R.id.nis)).perform(ViewActions.typeText("3868"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        onView(withId(R.id.cekuangsekolah)).perform(click())
        onView(withId(R.id.home2)).check(matches(isDisplayed()))
        ViewActions.pressBack()
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.nis)).perform(clearText())
        onView(withId(R.id.nis)).perform(ViewActions.typeText("3789"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        onView(withId(R.id.cekuangsekolah)).perform(click())
        onView(withId(R.id.home2)).check(matches(isDisplayed()))
        ViewActions.pressBack()
        onView(withId(R.id.nis)).perform(clearText())
        onView(withId(R.id.nis)).perform(ViewActions.typeText("3685"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        onView(withId(R.id.cekuangsekolah)).perform(click())
        onView(withId(R.id.home2)).check(matches(isDisplayed()))
    }

    //Testing untuk mengecek error pada NIS
    @Test
    fun checkError() {
        onView(withId(R.id.cekuangsekolah)).perform(click())
        onView(withId(R.id.nis)).check(matches(hasErrorText("Isi NIS terlebih dahulu")))
        onView(withId(R.id.nis)).perform(ViewActions.typeText("1234"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cekuangsekolah)).perform(click())
        onView(withId(R.id.nis)).check(matches(hasErrorText("NIS tidak terdaftar")))
    }

    //Testing untuk mengecek Toast pada saat NIS belum diisi
    @Test
    fun checkToastNISKosong() {
        onView(withId(R.id.cekuangsekolah)).perform(click())
        onView(withText("Isi NIS terlebih dahulu")).inRoot(withDecorView(not(`is`(activityTestRule.getActivity()
            .getWindow().getDecorView())))).check(matches(isDisplayed()))
    }

    //Testing untuk mengecek apakah Toast muncul pada saat NIS tidak terdaftar
    @Test
    fun checkToastNISTidakTerdaftar() {
        onView(withId(R.id.nis)).perform(ViewActions.typeText("1234"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cekuangsekolah)).perform(click())
        onView(withText("NIS tidak terdaftar")).inRoot(withDecorView(not(`is`(activityTestRule.getActivity()
            .getWindow().getDecorView())))).check(matches(isDisplayed()))
    }

    //Testing untuk mengecek fungsi button cek detail
    @Test
    fun clickCekDetail(){
        val activityScenario = ActivityScenario.launch(activity_home2::class.java)
        onView(withId(R.id.cekdetail)).perform(click())
        onView(withId(R.id.cekDetail)).check(matches(isDisplayed()))
    }
    // Testing berfungsinya OnSaveInstance saat orientasi berubah
    @Test
    fun testOnSaveInstance(){
        onView(withId(R.id.nis)).perform(typeText("3868"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        activityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        onView(withId(R.id.hiuser)).check(matches(withText("Hi, Marco")))
        activityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        onView(withId(R.id.hiuser)).check(matches(withText("Hi, Marco")))
        onView(withId(R.id.nis)).perform(clearText())
        onView(withId(R.id.nis)).perform(typeText("3789"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        activityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        onView(withId(R.id.hiuser)).check(matches(withText("Hi, Neveline")))
        activityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        onView(withId(R.id.hiuser)).check(matches(withText("Hi, Neveline")))
        onView(withId(R.id.nis)).perform(clearText())
        onView(withId(R.id.nis)).perform(typeText("3685"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        activityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        onView(withId(R.id.hiuser)).check(matches(withText("Hi, Anthony")))
        activityTestRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        onView(withId(R.id.hiuser)).check(matches(withText("Hi, Anthony")))
    }
    // Mempersiapkan semua yang diperlukan sebelum testing
    @Before
    fun setUp(){
        // Persiapan untuk menjalankan Intent
        Intents.init()
    }
    // Testing untuk mengecek saat ingin cek uang sekolah dengan data yang kosong tidak dapat berjalan
    @Test
    fun clickCekUangSklhNISKosong(){
        var person = Person("","","",0)
        onView(withId(R.id.cek)).perform(click())
        onView(withId(R.id.cekuangsekolah)).perform(click())
        not(intending(allOf(
                hasComponent(hasShortClassName(".activity_home2")),
                toPackage("com.example.checkpay"),
                hasExtra("123Person",person))
        ))
    }
    // Testing untuk mengecek saat klik cek uang sekolah dengan nis yang benar
    @Test
    fun clickCekUangSklhNISIsi(){
        onView(withId(R.id.nis)).perform(ViewActions.typeText("3868"))
        onView(withId(R.id.nis)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.cek)).perform(click())
        var person = Person(R.id.nis.toString(),R.id.nama.toString(),R.id.kelas.toString(),500000)
        onView(withId(R.id.cekuangsekolah)).perform(click())
        intending(allOf(
                hasComponent(hasShortClassName(".activity_home2")),
                toPackage("com.example.checkpay"),
                hasExtra("123Person",person))
        )
    }
    // Setelah testing selesai
    @After
    fun tearDown(){
        // Untuk penghematan memori, release intent yang sudah tidak digunakan
        Intents.release()
    }
}