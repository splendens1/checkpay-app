package com.example.checkpay

import android.provider.ContactsContract
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoginPresenterTest {

    var view = mock(ILoginView::class.java)
    var presenter = LoginPresenter(view)
    @Test
    fun emailKosong() {
        val result = presenter.onLogin("","")
        verify(view).onLoginError("Email tidak boleh kosong")
    }
    @Test
    fun emailSalah() {
        val result = presenter.onLogin("test","")
        verify(view).onLoginError("Email salah")
    }
    @Test
    fun passError() {
        val result = presenter.onLogin("splendens20@gmail.com","")
        verify(view).onLoginError("Password harus lebih dari 6 karakter")
    }
    @Test
    fun passSalah() {
        val result = presenter.onLogin("splendens20@gmail.com","12345678")
        verify(view).onLoginError("Password salah")
    }
    @Test
    fun loginBerhasil(){
        val result = presenter.onLogin("splendens20@gmail.com","checkpayapp")
        verify(view).onLoginSuccess("Login berhasil")
    }
}