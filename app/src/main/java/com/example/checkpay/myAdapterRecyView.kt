package com.example.checkpay

import android.net.Uri
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_recy_view.view.*

//Menambahkan implementasi RecyclerView.Adapter<myHolder>() yang wajib mengimplementasikan 3 metode :
//onCreateViewHolder(), getItemCount(), onBindViewHolder()
class myAdapterRecyView(private val contact: List<myContact>, val listener: myOnClickListener) : RecyclerView.Adapter<myAdapterRecyView.myHolder>(){
    inner class myHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val contactName = view.itemName
        private val contactNumber = view.itemNumber
        private val contactEmail = view.itemEmail
        private val itemPhoto = view.itemPhoto
        //function untuk binding dan menampilkan data
        fun bindContact(tmp: myContact){
            contactName.text ="${contactName.text} : ${tmp.nama}"
            contactNumber.text="${contactNumber.text} : ${tmp.nomorHp}"
            contactEmail.text="${contactEmail.text} : ${tmp.email}"
            if (tmp.photo!=null){
                //jika terdapat photo pada kontak maka ditampilkan jika tidak maka default
                itemPhoto.setImageBitmap(tmp.photo)
            }
        }
        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                listener.onClickListener(position)
            }
        }
    }
    //untuk menginflate holder dengan layout yang telah dibuat
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myHolder {
        return myHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.layout_recy_view,parent,false)
        )
    }
    //mengembalikan jumlah item dalam adapter
    override fun getItemCount(): Int = contact.size

    //melakukan binding data ke dalam holder yang telah dibuat
    override fun onBindViewHolder(holder: myHolder, position: Int) {
        holder.bindContact(contact[position])
    }

    interface myOnClickListener{
        fun onClickListener(position: Int){

        }
    }
}


