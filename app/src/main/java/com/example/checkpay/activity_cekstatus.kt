package com.example.checkpay

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_cekstatus.*

class activity_cekstatus : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cekstatus)
        var no = intent.getStringExtra(EXTRA_NO)
        if (no!=null){
            notelp.setText(no)
        }

        show.setOnClickListener{
            var intent = Intent(this,DetailContact::class.java)
            startActivity(intent)
        }
        kirimstatus.setOnClickListener {
            var intent = Intent(this,activity_statuspembayaran::class.java)
            intent.putExtra(EXTRA_NO,no)
            startActivity(intent)
        }
    }
}