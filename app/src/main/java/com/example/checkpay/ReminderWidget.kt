package com.example.checkpay

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import com.example.controlwidget.Widget2Pref

/**
 * Implementation of App Widget functionality.
 */
class ReminderWidget : AppWidgetProvider() {
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }
    companion object{
        private val PrefFileName = "Widget2Pref"
        internal fun updateAppWidget(
                context: Context,
                appWidgetManager: AppWidgetManager,
                appWidgetId: Int
        ) {
            var mySharedHelper = Widget2Pref(context,PrefFileName)
            var waktu = ""
            var isiPesan = mySharedHelper.pesan
            if (mySharedHelper.waktu==""){
                waktu="Buat pengingat"
                isiPesan="+"
            }else {
                waktu = "${mySharedHelper.waktu}"
            }
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.reminder_widget)
            views.setTextViewText(R.id.appwidget_text1, waktu)
            views.setTextViewText(R.id.appwidget_text2, isiPesan)
            views.setOnClickPendingIntent(R.id.layoutWidget, getPendingIntentActivity(context))

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
        private fun getPendingIntentActivity(context: Context): PendingIntent {
            var myIntent = Intent(context,pengingat::class.java)
            return PendingIntent.getActivity(context,0,myIntent,0)
        }
    }
}

