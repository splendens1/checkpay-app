package com.example.checkpay

import android.app.*
import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_cek_detail.*
import kotlinx.android.synthetic.main.activity_home2.*
import java.io.FileNotFoundException
import java.io.IOException

class activity_cek_detail : AppCompatActivity() {
    var mysqlitedb : myDBHelper? = null
    //Firebase
    lateinit var controller: FirebaseController
    lateinit var myAdapter: ArrayAdapter<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cek_detail)

        //Firebase
        controller = FirebaseController(this)

        //Pengambilan data Parcelable
        var p = intent.getParcelableExtra<Person>(EXTRA_PERSON)
        uangSekolah.text = "Rp. ${p?.Uang.toString()},-"
        diskon.text = "Rp. 0,-"
        denda.text = "Rp. 0,-"
        totalPembayaran.text = "Rp. ${p?.Uang.toString()},-"
        //Baca
        var nis = p?.NIS.toString()
        //Deklarasi database MySQLite
        mysqlitedb = myDBHelper(this)
        //Membaca field NIS pada database
        var ket = mysqlitedb!!.readUserT(nis)
        isiKeterangan.setText(ket)
//        ubah.setOnClickListener {
//            //Menjalankan transaction untuk mengupdate field keterangan
//            if(isiKeterangan.text.toString() != ket){
//                mysqlitedb!!.beginTransaction
//                try {
//                    val result = mysqlitedb!!.updateUserT(isiKeterangan.text.toString(),nis)
//                    if (result!=0){
//                        Toast.makeText(this,"Berhasil diubah",Toast.LENGTH_SHORT).show()
//                    }else{
//                        Toast.makeText(this,"Gagal",Toast.LENGTH_SHORT).show()
//                    }
//                }catch (e: SQLiteException){
//                    Log.e("Error",e.toString())
//                }finally {
//                    mysqlitedb!!.endTransaction
//                }
//            }
//        }
        //Membuat dan memanggil database dengan databaseBuilder
//        var db = Room.databaseBuilder(this,MyDBRoomHelper::class.java,"myroomdbex.db").build()
//        doAsync {
//            for (allData in db.SiswaDAO().getAllData()){
//                //baca keterangan berdasarkan nis siswa
//                if (allData.nis==nis) {
//                    isiKeterangan.text = allData.ket
//                }
//            }
//        }
//        readFile(p?.NIS.toString())

        //Bagian ALARM
        //mendeklarasikan sebuah Alarm Manager
        var myAlarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        //mendeklarasikan requestCode untuk menyatakan pendingIntent yang sama
        val requestCode = 1001
        //mendeklarasikan pendingIntent untuk menjalankan broadcast yang tidak bergantung pada mainActivity
        var myPendingIntent: PendingIntent? = null
        //mendeklarasikan sendIntent
        var sendIntent: Intent? = null

        //menampilkan notifikasi saat tombol lanjutkan di klik
        lanjutkan.setOnClickListener {
            //menjalankan intent
            var intent = Intent(this, com.example.checkpay.Bayar::class.java)
            var uang = totalPembayaran.text.toString()
            intent.putExtra(EXTRA_UANG,uang)
            startActivity(intent)
            //Menyimpan data siswa
            saveSiswa()

            //Bagian ALARM
            //membatalkan Alarm Manager
//                if (myPendingIntent != null) {
//                    myAlarmManager.cancel(myPendingIntent)
//                    myPendingIntent?.cancel()
//                    Toast.makeText(this, "Pengingat telah dihentikan",
//                            Toast.LENGTH_SHORT).show()
//                }

            //Bagian ALARM
//            btnAlarm.setOnClickListener {
//                //memastikan tidak ada alarm manager yang saling bentrok
//                if (myPendingIntent != null) {
//                    myAlarmManager.cancel(myPendingIntent)
//                    myPendingIntent?.cancel()
//                }
//
//                //memanggil broadcast pada intent
//                sendIntent = Intent(this, MyAlarmReceiver::class.java)
//                //memberikan pesan pada Alarm Manager
//                sendIntent?.putExtra(EXTRA_PESAN, "Silahkan lakukan Pembayaran")
//                //menampilkan broadcast pada intent
//                myPendingIntent = PendingIntent.getBroadcast(this, requestCode, sendIntent, 0)
//                //membuat alarm berulang setiap 15 menit sebelum alarm manager dihentikan
//                myAlarmManager.setInexactRepeating(
//                        AlarmManager.ELAPSED_REALTIME_WAKEUP,
//                        SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_FIFTEEN_MINUTES,
//                        AlarmManager.INTERVAL_FIFTEEN_MINUTES,
//                        myPendingIntent
//                )
//                Toast.makeText(this, "Pengingat akan muncul setiap 15 menit",
//                        Toast.LENGTH_SHORT).show()
//            }
        }
    }

    //Fungsi untuk menyimpan data siswa
    private fun saveSiswa() {
        controller.saveSiswa(nis.text.toString(), nama.text.toString(), kelas.text.toString(),
                uang.text.toString(), isiKeterangan.text.toString())
    }

    private fun readFile(nis: String) {
        try {
            var input = openFileInput("${nis}.txt")
                    .bufferedReader().useLines {
                        for (text in it.toList())
                            isiKeterangan.setText(text)
                    }
        }catch (e: FileNotFoundException){
            Toast.makeText(this, "Keterangan tidak ada", Toast.LENGTH_LONG).show()
        }catch (e : IOException){
            Toast.makeText(this, "Keterangan tidak dapat ditampilkan", Toast.LENGTH_LONG).show()
        }
    }
}