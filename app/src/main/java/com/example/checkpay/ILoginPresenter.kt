package com.example.checkpay

interface ILoginPresenter {
    fun onLogin (email:String, password:String)
}