package com.example.checkpay

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_statuspembayaran.*


class activity_statuspembayaran : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statuspembayaran)
        var no = intent.getStringExtra(EXTRA_NO)
        pesan.text="  TERIMA KASIH ^-^\n" +
                "  Status Pembayaran Anda\n" +
                "     Telah di Kirim ke\n" +
                "      ${no}"
        cetakbukti.setOnClickListener {
            var intent = Intent(this,BuktiPembayaran::class.java)
            startActivity(intent)
        }
    }
}