package com.example.checkpay

import android.graphics.Bitmap


data class myContact (
    var nama : String,
    var nomorHp : String,
    var email : String,
    var photo : Bitmap? = null
)