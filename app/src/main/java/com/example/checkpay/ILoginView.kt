package com.example.checkpay

import android.os.Message

interface ILoginView {
    fun onLoginSuccess(message: String)
    fun onLoginError(message: String)
}