package com.example.checkpay

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.sqlite.SQLiteException
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.StatFs
import android.text.format.Formatter
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home2.*
import kotlinx.android.synthetic.main.activity_home2.kelas
import kotlinx.android.synthetic.main.activity_home2.nama
import kotlinx.android.synthetic.main.activity_home2.nis
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.util.jar.Manifest
import kotlin.math.roundToInt

class activity_home2 : AppCompatActivity() {
    var mysqlitedb :  myDBHelper?= null
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home2)

        //Pengambilan data Parcelable dengan membuat objek berikut
        var p = intent.getParcelableExtra<Person>(EXTRA_PERSON)
        nis.text=p?.NIS
        nama.text=p?.Nama
        kelas.text=p?.Kelas
        uang.text="Uang Sekolah :"
        uangSklh.text = "Rp. ${p?.Uang.toString()},-"

        //Membuat dan memanggil database dengan databaseBuilder
        //var db = Room.databaseBuilder(this,MyDBRoomHelper::class.java,"myroomdbex.db").build()


//            var hasil = ""
//            doAsync {
//                //Ubah
//                //Mengecek jika siswa telah terdaftar maka isi keterangan diubah
//                if (db.SiswaDAO().isExist(nis.text.toString().toInt())) {
//                    db.SiswaDAO().editData(nis.text.toString().toInt(), keterangan.text.toString())
//                    uiThread {
//                        Toast.makeText(it,"Berhasil diubah",Toast.LENGTH_SHORT).show()
//                    }
        //Mendeklarasikan database MySQLite
        mysqlitedb = myDBHelper(this)
        cekdetail.setOnClickListener{
            //Mendeklarasikan muridTmp untuk menyimpan data sebelum ke database
            val muridTmp : Murid = Murid()
            muridTmp.nis = nis.text.toString()
            muridTmp.nama = nama.text.toString()
            muridTmp.kelas = kelas.text.toString()
            muridTmp.uangsklh = uangSklh.text.toString()
//            muridTmp.ket = keterangan.text.toString()
            //Menjalankan transaction
            mysqlitedb!!.beginTransaction
            try {
                var result = mysqlitedb?.addUserT(muridTmp)
                if(result!=-1L) {
                    Toast.makeText(this, "Berhasil", Toast.LENGTH_SHORT)
                }
                else{
                    Toast.makeText(this, "Gagal", Toast.LENGTH_SHORT)
                }
                mysqlitedb!!.successTransaction
            }catch (e: SQLiteException){
                Log.e("Error",e.toString())
            }finally {
                mysqlitedb!!.endTransaction
            }
//                //Tulis
//                else
//                {
//                    //data yang akan ditulis ke database dimasukkan ke dataclass
//                    val siswaTmp = Siswa(nis.text.toString().toInt())
//                    siswaTmp.nama = nama.text.toString()
//                    siswaTmp.kelas = kelas.text.toString()
//                    siswaTmp.uangsklh = uangSklh.text.toString()
//                    siswaTmp.ket = keterangan.text.toString()
//                    //menulis data nis, nama, kelas, uang sekolah dan keterangan ke database
//                    db.SiswaDAO().insertAll(siswaTmp)
//                    for (allData in db.SiswaDAO().getAllData()){
//                        hasil += "${allData}"
//                    }
//                    uiThread {
//                        Log.w("Hasil DB",hasil)
//                    }
//                }
//
//            }
            var intent = Intent(this, activity_cek_detail::class.java)
            var p2 = Person(nis.text.toString(),nama.text.toString(),kelas.text.toString(),500000)
            intent.putExtra(EXTRA_PERSON,p2)
            startActivity(intent)
        }

//        hapusData.setOnClickListener {
//            doAsync {
//                db.SiswaDAO().deleteAll(nis.text.toString().toInt())
//            }
//        }
//        inputKeterangan.setOnClickListener {
//            var hasil = ""
//            doAsync {
//                if (db.SiswaDAO().isExist(nis.text.toString().toInt())) {
//                    db.SiswaDAO().editData(nis.text.toString().toInt(), keterangan.text.toString())
//                    uiThread {
//                        Toast.makeText(it,"Berhasil diubah",Toast.LENGTH_SHORT).show()
//                    }
//                }
//                else
//                {
//                    val siswaTmp = Siswa(nis.text.toString().toInt())
//                    siswaTmp.nama = nama.text.toString()
//                    siswaTmp.kelas = kelas.text.toString()
//                    siswaTmp.uangsklh = uangSklh.text.toString()
//                    siswaTmp.ket = keterangan.text.toString()
//                    db.SiswaDAO().insertAll(siswaTmp)
//                    for (allData in db.SiswaDAO().getAllData()){
//                        hasil += "${allData}"
//                    }
//                    uiThread {
//                        Log.w("Hasil DB",hasil)
//                    }
//                }
//
//            }
//            writeFile()
//            if(isExternalStorageReadable()){
//                writeFileEksternal()
//            }
//            getStorage()
//        }

//        bacaKeterangan.setOnClickListener {
//            readFile()
//            if(isExternalStorageReadable()){
//                readFileEksternal()
//            }
//        }

//        hapusKeterangan.setOnClickListener {
//            deleteFile()
//            getStorage()
//            doAsync {
//                db.SiswaDAO().deleteAll(nis.text.toString().toInt())
//            }
//        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun isExternalStorageReadable():Boolean {
        if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED){
            requestPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),123)
        }
        var state = Environment.getExternalStorageState()
        if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)){
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            123 -> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this,"Izin diberikan",Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(this,"Izin belum diberikan",Toast.LENGTH_SHORT).show()
            }
        }
    }
    private fun deleteFile() {
        if(deleteFile("${nis.text}.txt")){
            Toast.makeText(this, "Keterangan Berhasil Dihapus", Toast.LENGTH_SHORT).show()
        }
        else
            Toast.makeText(this, "Keterangan Belum Ada", Toast.LENGTH_SHORT).show()
    }

//    private fun readFile() {
//        keterangan.text.clear()
//        try {
//            var input = openFileInput("${nis.text}.txt")
//                .bufferedReader().useLines {
//                    for (text in it.toList())
//                        keterangan.setText("${keterangan.text}\n$text")
//                }
//            Toast.makeText(this, "Keterangan berhasil ditampilkan", Toast.LENGTH_SHORT).show()
//        }catch (e: FileNotFoundException){
//            Toast.makeText(this, "Keterangan tidak ada", Toast.LENGTH_LONG).show()
//        }catch (e : IOException){
//            Toast.makeText(this, "Keterangan tidak dapat ditampilkan", Toast.LENGTH_LONG).show()
//        }
//    }

//    private fun writeFile() {
//        var ket = openFileOutput("${nis.text}.txt",
//        Context.MODE_PRIVATE).apply {
//            write(keterangan.text.toString().toByteArray())
//            close()
//        }
//        Toast.makeText(this, "Keterangan Berhasil Disimpan", Toast.LENGTH_SHORT).show()
//    }

//    private fun writeFileEksternal() {
//        var myDir = File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)?.toURI())
//        if (!myDir.exists()){
//            myDir.mkdir()
//        }
//        File(myDir,"${nis.text}.txt").apply {
//            writeText(keterangan.text.toString())
//        }
//    }
//    private fun readFileEksternal() {
//        var myDir = File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)?.toURI())
//        keterangan.text.clear()
//        var readfile = ""
//        File(myDir,"${nis.text}.txt").forEachLine {
//            readfile+=it
//        }
//        keterangan.setText(readfile)
//    }

    private fun getStorage() {
        val stat = StatFs(Environment.getDataDirectory().path)
        val path: File = Environment.getDataDirectory()
        val blockSize = stat.blockSizeLong
        val availableBlocks = stat.availableBlocksLong
        val format: String = Formatter.formatFileSize(this, availableBlocks * blockSize)
        Toast.makeText(this,String.format("Memori tersisa : %s", format),Toast.LENGTH_SHORT).show()
    }
    //Fungsi untuk intent implisit
    fun share(view: View) {
        val mimeType = "text/plain"
        ShareCompat.IntentBuilder
                .from(this)
                .setType(mimeType)
                .setChooserTitle("Bagikan dengan aplikasi : ")
                .setText(uang.text.toString())
                .startChooser()
    }


}