package com.example.checkpay

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CalendarView
import android.widget.Toast
import androidx.core.view.isVisible
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpClient.log
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_home.*
import org.jetbrains.anko.toast
import org.json.JSONArray
import java.text.SimpleDateFormat
import java.util.*

//Deklarasi variabel onSaveInstanceState
private  const val EXTRA_HI = "EXTRA_HI"
private const val EXTRA_NAMA = "EXTRA_NAMA"
private const val EXTRA_KELAS = "EXTRA_KELAS"
//Deklarasi Preference File Name
private val PrefFileName = "MYFILEPREF01"

class activity_home : AppCompatActivity() {
    val JobSchedulerId = 10
    lateinit var controller: FirebaseController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        controller = FirebaseController(this)
        //Deklarasi mySharedHelper
        var mySharedHelper = SharePrefHelper(this,PrefFileName)
        cek.setOnClickListener {
            val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val date = sdf.format(Date())
            if (nis.text.toString()=="3868"){
                nama.text="Marco"
                kelas.text="A"
                hiuser.text="Hi, ${nama.text}"
                mySharedHelper.nis = nis.text.toString()
                mySharedHelper.nama = nama.text.toString()
                mySharedHelper.kelas = kelas.text.toString()
                mySharedHelper.tgl = date

                hiuser.isVisible = true
                namatxt.isVisible = true
                nama.isVisible = true
                kelastxt.isVisible = true
                kelas.isVisible = true
                cekuangsekolah.isVisible = true
            }
            else if (nis.text.toString()=="3789"){
                nama.text="Neveline"
                kelas.text="B"
                hiuser.text="Hi, ${nama.text}"
                mySharedHelper.nis = nis.text.toString()
                mySharedHelper.nama = nama.text.toString()
                mySharedHelper.kelas = kelas.text.toString()
                mySharedHelper.tgl = date

                hiuser.isVisible = true
                namatxt.isVisible = true
                nama.isVisible = true
                kelastxt.isVisible = true
                kelas.isVisible = true
                cekuangsekolah.isVisible = true
            }
            else if (nis.text.toString()=="3685"){
                nama.text="Anthony"
                kelas.text="C"
                hiuser.text="Hi, ${nama.text}"
                mySharedHelper.nis = nis.text.toString()
                mySharedHelper.nama = nama.text.toString()
                mySharedHelper.kelas = kelas.text.toString()
                mySharedHelper.tgl = date

                hiuser.isVisible = true
                namatxt.isVisible = true
                nama.isVisible = true
                kelastxt.isVisible = true
                kelas.isVisible = true
                cekuangsekolah.isVisible = true
            }
            else
            {
                nama.text=""
                kelas.text=""
                hiuser.text="Hi, ${nama.text}"
            }
        }
        cekInfoCovid.setOnClickListener {
            var intent = Intent(this, info_covid::class.java)
            startActivity(intent)
        }
        //Menjalankan intent pada saat tombol cek terakhir di klik
//        cekTerakhir.setOnClickListener {
//            var intent = Intent(this,activity_riwayat::class.java)
//            startActivity(intent)
//        }
        cekuangsekolah.setOnClickListener {
            var intent = Intent(this,activity_home2::class.java)

            if (nis.text.toString()==""){
                Toast.makeText(this,"Isi NIS terlebih dahulu",Toast.LENGTH_SHORT).show()
                nis.error="Isi NIS terlebih dahulu"
            }
            else if (nama.text=="" && kelas.text=="") {
                Toast.makeText(this,"Siswa tidak terdaftar",Toast.LENGTH_SHORT).show()
                nis.error="NIS tidak terdaftar"
            }
            else {
                //Pengiriman class Person menggunakan Parcelable
                var p = Person(nis.text.toString(),nama.text.toString(),kelas.text.toString(),500000)
                intent.putExtra(EXTRA_PERSON,p)
                startActivity(intent)
            }
        }
    }

    //Fungsi untuk menyimpan data saat terjadi perubahan State
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(EXTRA_HI,hiuser.text.toString())
        outState.putString(EXTRA_NAMA,nama.text.toString())
        outState.putString(EXTRA_KELAS,kelas.text.toString())
    }

    //Fungsi untuk mengembalikan data yang sudah disimpan sebelumnya
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        hiuser.text=savedInstanceState?.getString(EXTRA_HI) ?: "Hi, ..."
        nama.text=savedInstanceState?.getString(EXTRA_NAMA) ?: "No Name"
        kelas.text=savedInstanceState?.getString(EXTRA_KELAS) ?: "Kosong"
    }
}


