package com.example.checkpay

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.widget.Toast
import java.lang.Exception

class SendMyService : Service() {

    override fun onBind(intent: Intent): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //Memunculkan pesan "Download dimulai" saat service Download dimulai
        Toast.makeText(this, "Download dimulai", Toast.LENGTH_SHORT).show()
        //Membuat thread untuk memperlihatkan service Download
        Thread(Runnable {
            for (i in 0..10)
                try {
                    Thread.sleep(500L)
                }
                catch (e: Exception){
                }
            stopSelf()
        }).start()
        return START_STICKY
    }

    override fun onDestroy() {
        //Memunculkan pesan "Download selesai" daat service Download selesai dilakukan
        Toast.makeText(this, "Download selesai", Toast.LENGTH_SHORT).show()
    }
}
