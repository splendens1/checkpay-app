package com.example.checkpay

import android.util.Patterns
import cz.msebera.android.httpclient.util.TextUtils
import org.w3c.dom.Text
import java.util.regex.Pattern

class User(override val email: String, override val password:String):IUser {
    val EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+")
    override fun isDataValid(): Int {
        if(email.isEmpty())
            return 0
        else if(!EMAIL_ADDRESS_PATTERN.matcher(email).matches())
            return 1
        else if(password.length <= 6)
            return 2
        else if(password!="checkpayapp")
            return 3
        else
            return -1
    }
}