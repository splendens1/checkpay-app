package com.example.checkpay

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

//Membuat interface DAO
@Dao
interface SiswaDAO {
    //Query untuk membaca semua data dari table siswa
    @Query("Select * From Siswa")
    fun getAllData() : List<Siswa>

    //Query untuk insert data ke table siswa
    @Insert
    fun insertAll(vararg siswa: Siswa)

    //Query untuk menghapus data di table siswa sesuai nis
    @Query("Delete from siswa where nis = :nis")
    fun deleteAll(nis : Int)

    //Query untuk mengecek apakah data siswa sudah ada
    @Query("SELECT EXISTS(SELECT * FROM Siswa where nis = :nis)")
    fun isExist(nis : Int) : Boolean

    //Query untuk mengubah isi keterangan siswa berdasarkan nis
    @Query("UPDATE Siswa SET COLUMN_Ket=:ket where nis=:nis")
    fun editData(nis: Int, ket : String)
}