package com.example.checkpay

import android.app.job.JobParameters
import android.app.job.JobService
import android.util.Log
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONArray

class InfoCovid : JobService() {

    //function yang akan dijalankan ketika scheduler telah aktif sesuai jadwal yang telah ditentukan
    override fun onStartJob(params: JobParameters?): Boolean {
        Log.w("Job", "Mulai")
        //dijalankan dithread yang berbeda
        return true
    }
    //dijalankan ketika scheduler sedang berjalan tetapi berhenti ketika kondisi tidak terpenuhi
    override fun onStopJob(params: JobParameters?): Boolean {
        Log.w("Job", "Berhenti")
        //ketika gagal dijalankan maka langsung stop tidak direschedule
        return false
    }
}

//    private fun getCuacaHariIni(jobParameters: JobParameters?) {
//        var client = AsyncHttpClient()
//        var url = "https://api.kawalcorona.com/indonesia"
//        val charset = Charsets.UTF_8
//        var handler = object : AsyncHttpResponseHandler(){
//            override fun onSuccess(
//                    statusCode: Int,
//                    headers: Array<out Header>?,
//                    responseBody: ByteArray?
//            ) {
//                result = responseBody?.toString(charset) ?: "Kosong"
//                val positif = JSONArray(result).getJSONObject(0).getString("positif")
//                val sembuh = JSONArray(result).getJSONObject(0).getString("sembuh")
//                val meninggal = JSONArray(result).getJSONObject(0).getString("meninggal")
//                val dirawat = JSONArray(result).getJSONObject(0).getString("dirawat")
//                Log.w ("Hasil",result)
//                Log.w ("Hasil",positif)
//                Log.w ("Hasil",sembuh)
//                Log.e ("Hasil",meninggal)
//                Log.e ("Hasil",dirawat)
//                jobFinished(jobParameters,false)
//            }
//
//            override fun onFailure(
//                    statusCode: Int,
//                    headers: Array<out Header>?,
//                    responseBody: ByteArray?,
//                    error: Throwable?
//            ) {
//                jobFinished(jobParameters,true)
//            }
//        }
//        client.get(url,handler)
//    }
//}