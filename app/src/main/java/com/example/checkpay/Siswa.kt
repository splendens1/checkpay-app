package com.example.checkpay

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

//Membuat data class entity siswa untuk membuat table siswa pada database
@Entity
data class Siswa(
    @PrimaryKey val nis: String,
    @ColumnInfo(name = "COLUMN_Nama") var nama: String = "",
    @ColumnInfo(name = "COLUMN_Kelas") var kelas: String = "",
    @ColumnInfo(name = "COLUMN_UangSklh") var uangsklh: String = "",
    @ColumnInfo(name = "COLUMN_Ket") var ket: String = ""
)
