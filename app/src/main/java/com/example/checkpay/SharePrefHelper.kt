package com.example.checkpay

import android.content.Context
import android.content.SharedPreferences

class SharePrefHelper(context: Context, name: String) {
    //Inisialisasi Key dan Value
    val USER_NIS = "NIS"
    val USER_NAMA = "NAMA"
    val USER_KELAS = "KELAS"
    val USER_TGL = "TGL"
    val USER_PASS = "PASS"
    val USER_EMAIL = "EMAIL"

    //Deklarasi SharedPreference
    private var myPreferences: SharedPreferences
    init {
        myPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    //Membuat fungsi untuk SharedPreference
    inline fun SharedPreferences.editMe(opertion: (SharedPreferences.Editor) -> Unit) {
        val editMe = edit()
        opertion(editMe)
        editMe.apply()
    }
    //Menyimpan data NIS
    var nis : String?
        get() = myPreferences.getString(USER_NIS, "-")
        set(value) {
            myPreferences.editMe {
                it.putString(USER_NIS, value)
            }
        }
    //Menyimpan data Nama
    var nama : String?
        get() = myPreferences.getString(USER_NAMA, "-")
        set(value) {
            myPreferences.editMe {
                it.putString(USER_NAMA, value)
            }
        }
    //Menyimpan data Kelas
    var kelas : String?
        get() = myPreferences.getString(USER_KELAS, "-")
        set(value) {
            myPreferences.editMe {
                it.putString(USER_KELAS, value)
            }
        }
    //Menyimpan data Tanggal
    var tgl : String?
        get() = myPreferences.getString(USER_TGL, "")
        set(value) {
            myPreferences.editMe {
                it.putString(USER_TGL, value)
            }
        }
    //Menyimpan data Email
    var email : String?
        get() = myPreferences.getString(USER_EMAIL, "")
        set(value) {
            myPreferences.editMe {
                it.putString(USER_EMAIL, value)
            }
        }
    //Menyimpan data Password
    var pass : String?
        get() = myPreferences.getString(USER_PASS, "")
        set(value) {
            myPreferences.editMe {
                it.putString(USER_PASS, value)
            }
        }
    //Fungsi untuk menghapus data
    fun clearValues() {
        myPreferences.editMe {
            it.clear()
        }
    }
}