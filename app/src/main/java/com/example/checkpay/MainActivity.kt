package com.example.checkpay

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.*
import android.content.res.Configuration
import android.net.Uri
import android.net.Uri.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.provider.Telephony
import android.util.Log
import android.widget.MediaController
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_info_covid.*
import java.util.*

class MainActivity : AppCompatActivity(){

    private var allow = false
    private var airplaneReceiver = object : BroadcastReceiver(){
        //onReceive akan dijalankan ketika status dari flightmode sistem android berubah
        override fun onReceive(context: Context?, intent: Intent?) {
            //Pengecekan flightmode dalam kondisi hidup atau tidak
            if(Settings.System.getInt(contentResolver, Settings.Global.AIRPLANE_MODE_ON,0)==0) {
                allow = false
                Toast.makeText(context, "Airplane Mode: OFF", Toast.LENGTH_LONG).show()
            }
            else {
                allow = true
                Toast.makeText(context, "Airplane Mode : ON", Toast.LENGTH_LONG).show()
                System.exit(-1)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Mendaftarkan filter untuk menerima perubahan status airplane mode
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED)
        //Proses register receiver
        registerReceiver(airplaneReceiver,filter)

        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this,activity_home::class.java))
            finish()
        },1000)

//        var alarmIntent = Intent(this, Widget1::class.java).let {
////            it.action = Widget1.ACTION_AUTO_UPDATE
//            PendingIntent.getBroadcast(
//                this, 101, it, PendingIntent.FLAG_UPDATE_CURRENT
//            )
//        }
        var cal = Calendar.getInstance()
        cal.add(Calendar.MINUTE, 1)

//        var alarmManager = this. getSystemService(Context.ALARM_SERVICE) as AlarmManager
//        alarmManager.setRepeating(AlarmManager.RTC, cal.timeInMillis, 300000L, alarmIntent)
//        Log.e("Alarm Start", "Alarm Start")
    }
}