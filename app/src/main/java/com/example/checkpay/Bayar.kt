package com.example.checkpay

import android.app.NotificationChannel
import android.app.NotificationChannelGroup
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.TaskStackBuilder
import com.example.checkpay.Bayar
import kotlinx.android.synthetic.main.activity_bayar.*
import kotlinx.android.synthetic.main.activity_cek_detail.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import kotlinx.android.synthetic.main.activity_bayar.totalPembayaran as totalPembayaran1

class Bayar : AppCompatActivity() {
    private var CHANNEL_ID = "Channel_ID"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bayar)

        var uang = intent.getStringExtra(EXTRA_UANG)
        totalPembayaran.text = "${uang}"

        Bayar.setOnClickListener {
            //menjalankan intent
            var intent = Intent(this, activity_konfirmasi::class.java)
            startActivity(intent)
            doAsync {
                Thread.sleep(5000L)
                uiThread {
                    showNotify()
                }
            }
        }

        //memanggil fungsi Notification Channel
        CreateNotificationChannel()
        //memanggil fungsi Notification Group
        CreateNotificationGroup()
    }
    private fun showNotify(){
        //membuat judul notifikasi
        val title = "Notifikasi Pembayaran"
        //membuat deskripsi notifikasi
        val desc = "Selamat! Pembayaran anda berhasil!"
        //mendeklarasikan notificationLayout dengan memanggil custom_notif.xml
        val notificationLayout = RemoteViews(packageName,R.layout.custom_notif)
        notificationLayout.setTextViewText(R.id.notifTitle,title)
        notificationLayout.setTextViewText(R.id.notifDesc,desc)
        //menampilkan logo aplikasi pada notifikasi
        notificationLayout.setImageViewResource(R.id.notifImg,R.drawable.logo)
        //mendeklarasikan intent
        val resultIntent=Intent(this,activity_konfirmasi::class.java)
        //mendeklarasikan Pending Intent yang memanfaatkan API TaskStackBuilder untuk mengatur BackStack
        val resultPendingIntent: PendingIntent?= TaskStackBuilder.create(this).run {
            //untuk menambahkan isi backstack dengan parent dari pemanggil
            addNextIntentWithParentStack(resultIntent)
            //menampilkan task yang telah dibuat dengan mengirimkan kode unik dan sebuah flag
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }
        //membuat notifikasi
        val builder = androidx.core.app.NotificationCompat.Builder(this,CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_baseline_payment_24)
                .setStyle(androidx.core.app.NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayout)
                .setPriority(androidx.core.app.NotificationCompat.PRIORITY_DEFAULT)
                .setGroup("Pembayaran")
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(desc)
                .setSmallIcon(R.drawable.ic_baseline_payment_24)


        //menampilkan notifikasi melalui Notification Manager
        with(NotificationManagerCompat.from(this)){
            notify(0,builder.build())
        }
    }

    //fungsi untuk membuat notification channel
    private fun CreateNotificationChannel(){
        //mengecek versi android >= Oreo
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            //membuat URI sebagai sumber daya sound
            val notifSound = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            //membentuk atribut sound
            val att = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()
            //mendeklarasikan nama notifikasi
            val name = "Notifikasi Pembayaran"
            //menentukan kepentingan dari notifikasi
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            //menentukan warna lampu indikator saat ada notifikasi
            val channel = NotificationChannel(CHANNEL_ID,name,importance).apply {
                lightColor = Color.GREEN
            }
            //memunculkan sound notification
            channel.setSound(notifSound,att)
            //mendeklarasikan notification manager
            val notificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            //mendaftarkan notification channel ke dalam notification manager
            notificationManager.createNotificationChannel(channel)
        }
    }

    //fungsi untuk notification group
    private fun CreateNotificationGroup(){
        //mengecek versi android >= Oreo
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            //mendeklarasikan group id
            val groupID = "Pembayaran"
            //mendeklarasikan group name
            val groupName = "Pembayaran"
            //mendeklarasikan notification manager
            val notificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            //mendaftarkan notification group ke dalam notification manager
            notificationManager.createNotificationChannelGroup(NotificationChannelGroup(groupID,groupName))
        }
    }
}