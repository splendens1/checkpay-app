package com.example.checkpay

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

//Membuat data Parcelable Murid
@Parcelize
class Murid : Parcelable {
    var id : Int = 0
    var nis : String = ""
    var nama : String = ""
    var kelas : String = ""
    var uangsklh : String = ""
    var ket : String = ""
}