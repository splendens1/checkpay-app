package com.example.checkpay

import android.content.Context
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class FirebaseController(context: Context) {
    //Pemanggilan Firebase Database
    private var database = Firebase.database
    //Mengakses Database dengan nama Table SISWA
    private val ref = database.getReference("SISWA")
    private var context = context
    private var hasil = mutableMapOf<String, String>()

    //Fungsi untuk menyimpan data siswa
    fun saveSiswa(nis: String, nama: String, kelas: String, uangsklh: String, ket: String){
        val IDSiswa = ref.push().key.toString()
        val DataSiswa = SiswaFB(nis, nama, kelas, uangsklh, ket)
        ref.child(IDSiswa).setValue(DataSiswa).apply {
            addOnCompleteListener {
                Toast.makeText(context,"Data Tersimpan", Toast.LENGTH_SHORT).show()
            }
            addOnFailureListener {
                Toast.makeText(context, "${it.message}", Toast.LENGTH_SHORT).show()
            }
            addOnCanceledListener {  }
            addOnSuccessListener {  }
        }
    }

    //Fungsi untuk membaca data siswa
    private fun readSiswa() : MutableMap<String, String> {
        ref.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }
            override fun onDataChange(p0: DataSnapshot) {
                if(p0!!.exists()){
                    hasil.clear()
                    for (data in p0.children){
                        val siswa = data.getValue(SiswaFB::class.java)
                        siswa?.let { hasil.put(data.key.toString(),it.NIS) }
                    }
                }
            }
        })
        return hasil
    }

    //Fungsi untuk mengecek apakah memiliki data yang sama
    fun isExist(srcName : String) : Boolean{
        if(readSiswa().containsValue(srcName))
            return true
        return false
    }
}
