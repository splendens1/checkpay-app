package com.example.checkpay

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.OnUserEarnedRewardListener
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_info_covid.*
import org.json.JSONArray

class info_covid : AppCompatActivity() {
    val JobSchedulerId = 10
    private var mInterAds : InterstitialAd? = null
    private var mRewardVid : RewardedAd? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_covid)
        startMyJob()

        //membuat banner ads
        MobileAds.initialize(this) {}
        adView.loadAd(AdRequest.Builder().build())
        adView.adListener = object : AdListener(){
        }

        //membuat interstitial ads
        InterstitialAd.load(this,"ca-app-pub-3940256099942544/1033173712",
            AdRequest.Builder().build(), object : InterstitialAdLoadCallback(){
                //Fungsi yang dijalankan ketika iklan gagal di load
                override fun onAdFailedToLoad(p0: LoadAdError) {
                    Toast.makeText(this@info_covid,"Load Failed",Toast.LENGTH_SHORT).show()
                    mInterAds = null
                }
                //Fungsi yang dijalankan ketika iklan di load
                override fun onAdLoaded(p0: InterstitialAd) {
                    super.onAdLoaded(p0)
                    mInterAds = p0
                    mInterAds?.show(this@info_covid)
                }
            })

        //membuat Media Player dengan menekan tombol play
        playvideo.setOnClickListener{
            var intent = Intent(this,media_player::class.java)
            startActivity(intent)
        }
    }
    //fungsi untuk mendapatkan data dari API
    public fun getCovidInfo() {
        //membuat objek AsyncHttpClient() yang akan memproses penerimaan data dari Internet secara Async melalui AsyncHttpResponseHandler()
        var client = AsyncHttpClient()
        //url yang akan digunakan ketika meminta data dari API
        var url = "https://api.kawalcorona.com/indonesia"
        //menentukan tipe charset
        val charset = Charsets.UTF_8
        var handler = object : AsyncHttpResponseHandler(){
            //dijalankan ketika data dari API berhasil diambil
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?
            ) {
                //mengubah hasil menjadi string sesuai charset (json)
                val result = responseBody?.toString(charset) ?: "Kosong"
                //mengubah jsonarray menjadi string dan ditampilkan di textview
                positif.setText(JSONArray(result).getJSONObject(0).getString("positif"))
                sembuh.setText(JSONArray(result).getJSONObject(0).getString("sembuh"))
                meninggal.setText(JSONArray(result).getJSONObject(0).getString("meninggal"))
                dirawat.setText(JSONArray(result).getJSONObject(0).getString("dirawat"))
            }
            //dijalankan ketika data dari API gagal diambil
            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?,
                error: Throwable?
            ) {
                Log.w("Info", "Gagal")
            }
        }
        //mengambil data melalui AsyncHttpClient() dengan fungsi get() dengan parameter url dan objek AsyncHttpResponseHandler()
        client.get(url,handler)
    }
    //mendaftarkan job scheduler yang akan digunakan sebagai service
    private fun startMyJob() {
        var serviceComponent = ComponentName(this,InfoCovid::class.java)
        //membentuk jobinfo untuk job scheduler
        val mJobInfo = JobInfo.Builder(JobSchedulerId, serviceComponent)
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY) //menentukan jenis koneksi internet
            .setRequiresDeviceIdle(false) //menentukan apakah job dapat dijalankan dalam kondisi idle
            .setRequiresCharging(false) //menentukan apakah job dapat dijalankan ketika dalam keadaan charging
            .setPeriodic(15 * 60 * 1000) //menentukan kapan job akan diulang

        //membentuk objek job scheduler
        var JobInfo = this.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        JobInfo.schedule(mJobInfo.build())
        Toast.makeText(this,"Job Service Berjalan", Toast.LENGTH_SHORT).show()
        getCovidInfo()
    }
}