package com.example.checkpay

interface IUser {
    val email:String
    val password:String
    fun isDataValid() : Int
}
