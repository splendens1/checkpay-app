package com.example.controlwidget

import android.content.Context
import android.content.SharedPreferences

class Widget2Pref(context: Context, name: String) {
    val WAKTU = "WAKTU"
    val PESAN = "PESAN"

    private var myPreferences: SharedPreferences

    init {
        myPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    inline fun SharedPreferences.editMe(opertion: (SharedPreferences.Editor) -> Unit) {
        val editMe = edit()
        opertion(editMe)
        editMe.apply()
    }

    var waktu: String?
        get() = myPreferences.getString(WAKTU, "")
        set(value) {
            myPreferences.edit().putString(WAKTU, value).apply()
        }
    var pesan: String?
        get() = myPreferences.getString(PESAN, "")
        set(value) {
            myPreferences.edit().putString(PESAN, value).apply()
        }

    fun clearValues() {
        myPreferences.edit().clear().apply()

    }
}