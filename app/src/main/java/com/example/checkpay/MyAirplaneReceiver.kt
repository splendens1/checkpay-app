package com.example.checkpay

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.widget.Toast

class MyAirplaneReceiver : BroadcastReceiver() {

    //onReceive akan dijalankan ketika status dari flightmode sistem android berubah
    override fun onReceive(context: Context, intent: Intent) {
        //Pengecekan apabila flightmode dalam kondisi hidup atau mati
        if (Settings.System.getInt(context.contentResolver,
                        Settings.Global.AIRPLANE_MODE_ON,0)==0){
            //Memunculkan pesan "Flight Mode : OFF" saat flightmode dalam kondisi mati
            Toast.makeText(context,"Flight Mode : OFF", Toast.LENGTH_SHORT).show()
        }
        else {
            //Memunculkan pesan "Flight Mode : ON" saat flightmode dalam kondisi hidup
            Toast.makeText(context,"Flight Mode : ON", Toast.LENGTH_SHORT).show()
            System.exit(-1)
        }
    }
}