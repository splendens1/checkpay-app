package com.example.checkpay

import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.controlwidget.Widget2Pref
import com.example.pertemuan5.MyReminderReceiver
import kotlinx.android.synthetic.main.activity_pengingat.*
import java.util.*

private val PrefFileName = "Widget2Pref"
class pengingat : AppCompatActivity() {
    private var mPendingIntent: PendingIntent? = null
    private var sendIntent: Intent? = null
    private var mAlarmManager: AlarmManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        var mySharedHelper = Widget2Pref(this,PrefFileName)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pengingat)

        mAlarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        setAlarm.setOnClickListener {
            mySharedHelper.waktu = "${datePicker.dayOfMonth}/${datePicker.month+1}/${datePicker.year}"
            mySharedHelper.pesan = myMessage.text.toString()
            if(mPendingIntent!=null){
                mAlarmManager?.cancel(mPendingIntent)
                mPendingIntent?.cancel()
            }
            var setAlarmTime = Calendar.getInstance()
            setAlarmTime.set(Calendar.DAY_OF_MONTH,datePicker.dayOfMonth)
            setAlarmTime.set(Calendar.MONTH,datePicker.month+1)
            setAlarmTime.set(Calendar.YEAR,datePicker.year)
            sendIntent?.putExtra(EXTRA_PESAN,myMessage.text.toString())
            var sendIntent = Intent(this, MyReminderReceiver::class.java)
            var mPendingIntent = PendingIntent.getBroadcast(this,101,sendIntent,0)
            mAlarmManager?.set(AlarmManager.RTC_WAKEUP,setAlarmTime.timeInMillis,mPendingIntent)
            Toast.makeText(this,"Pengingat dibuat untuk tanggal ${datePicker.dayOfMonth}/${datePicker.month+1}/${datePicker.year}", Toast.LENGTH_SHORT).show()
        }

    }
    override fun onStop() {
        super.onStop()
        var appWidgetManager = AppWidgetManager.getInstance(this)
        var ids = appWidgetManager.getAppWidgetIds(ComponentName(this,ReminderWidget::class.java))
        var updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids)
        sendBroadcast(updateIntent)
    }

    private fun diffDays(): Long {
        val day = datePicker.dayOfMonth
        val month = datePicker.month
        val year = datePicker.year
        val calendar = Calendar.getInstance()
        calendar.set(year,month,day)
        val diff = calendar.timeInMillis- Calendar.getInstance().timeInMillis
        val diffDays = diff / (24 * 60 * 60 * 1000)
        return diffDays
    }
}