package com.example.checkpay

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Telephony
import android.telephony.SmsMessage
import android.widget.Toast
import androidx.annotation.RequiresApi

class MySMSReceiver : BroadcastReceiver() {

    @RequiresApi(Build.VERSION_CODES.M)
    //onReceive akan dijalankan ketika ada status dari SMS yang masuk
    override fun onReceive(context: Context, intent: Intent) {
        if(intent.action.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)){
            var pdu = (intent.extras!!.get("pdus") as Array<*>).get(0)
            //Deklarasi Bundle
            var myBundle = intent.extras
            //Deklarasi format untuk mengambil pesan pada Bundle
            var format = myBundle!!.getString("format")
            pdu.let{
                var message = SmsMessage.createFromPdu(it as ByteArray, format)
                //Deklarasi pesan untuk menyimpan isi dari pesan
                var pesan = message.displayMessageBody
                //Deklarasi no_Pengirim untuk menyimpan data pengirim
                var no_Pengirim =message.displayOriginatingAddress
                //Memunculkan pesan dari variable pesan dan no_Pengirim
                Toast.makeText(context, "Phone : $no_Pengirim \n" +
                        "Message : $pesan", Toast.LENGTH_SHORT).show()
            }
        }
    }
}