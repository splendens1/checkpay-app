package com.example.checkpay

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_konfirmasi.*


class activity_konfirmasi : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_konfirmasi)

        cekstatus.setOnClickListener {
            var intent = Intent(this,activity_cekstatus::class.java)
            startActivity(intent)
        }
        cetakbukti.setOnClickListener {
            var intent = Intent(this,BuktiPembayaran::class.java)
            startActivity(intent)
        }
    }
}