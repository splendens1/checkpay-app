package com.example.checkpay

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_bukti_pembayaran.*
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class BuktiPembayaran : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bukti_pembayaran)

        //deklarasi komponen pada thread
        val viewImg = findViewById<ImageView>(R.id.imgView)
        var strUrl = "https://i.ibb.co/qJ6WT4n/Group-9.png"

        /*mendeklarasikan handler sehingga semua thread lain
        terhubung melalui handler ini
         */
        val handler = object : Handler(Looper.getMainLooper()){
            /*untuk menerima semua data dari thread lain
            dalam bentuk object message
             */
            override fun handleMessage(inputMessage : Message){
                val photoTask = inputMessage.obj as Bitmap
                viewImg.setImageBitmap(photoTask)
            }
        }

        //menciptakan thread baru untuk menjalankan fungsi di dalamnya
        Thread({
            val bitmap = processBitmap(strUrl)
            /* menentukan handler mana yang akan dikirimkan pesan */
            val msg = Message.obtain(handler)
            /* pesan yang dikirimkan berupa object yaitu bitmap
            yang merupakan processbitmap dari url */
            msg.obj = bitmap
            /* mengirim data ke handler */
            msg.sendToTarget()
        }).start()


        //Deklarasi myService untuk memanggil service
        var myService = Intent(this, SendMyService::class.java)
        download.setOnClickListener{
            startService(myService)
            myProgress.visibility = View.VISIBLE

            Handler().postDelayed({
                myProgress.visibility = View.GONE
            },5000)
        }
    }

    //fungsi permintaan gambar dari server
    private fun processBitmap(url: String): Bitmap? {
        return try {
            var url = URL(url)
            val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input: InputStream = connection.inputStream
            val myBitmap = BitmapFactory.decodeStream(input)
            myBitmap
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }
}