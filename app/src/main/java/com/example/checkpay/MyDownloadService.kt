package com.example.checkpay

import android.content.Intent
import android.content.Context
import android.widget.Toast
import androidx.core.app.JobIntentService


class MyDownloadService : JobIntentService() {

    override fun onHandleWork(intent: Intent) {
        var downloadProgress = 0
        do{
            Thread.sleep(100)
            downloadProgress+=1
            var intentFileDownload = Intent(ACTION_DOWNLOAD)
            intentFileDownload.putExtra(EXTRA_PERSEN, downloadProgress)
            intentFileDownload.putExtra(EXTRA_FINISH, false)
            if(downloadProgress>=100)
                intentFileDownload.putExtra(EXTRA_FINISH, true)
            sendBroadcast(intentFileDownload)
        }while (downloadProgress<100)
    }

    override fun onDestroy() {
        super.onDestroy()
        /*Membuat toast saat service dihentikan atau telah
        menyelesaikan proses service tersebut dengan pesan
        "Status Pembayaran Berhasil Dikirim
         */
        Toast.makeText(this, "Download selesai", Toast.LENGTH_SHORT).show()
    }

    companion object {
        fun enqueueWork(context: Context, intent: Intent)
        {
            enqueueWork(context, MyDownloadService::class.java, JOB_ID_DOWNLOAD, intent)
        }
    }
}