package com.example.checkpay

import android.content.Intent
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_login.*

//Deklarasi SoundPool dan soundID
private var sp: SoundPool?= null
private var soundID = 0


private val PrefFileName = "MYFILEPREF01"

class activity_login : AppCompatActivity(), ILoginView {
    override fun onLoginSuccess(message: String) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
        var intent = Intent(this, activity_home::class.java)
        startActivity(intent)
    }
    override fun onLoginError(message: String) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }
    internal lateinit var loginPresenter: ILoginPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        var mySharedHelper = SharePrefHelper(this,PrefFileName)
        email.setText(mySharedHelper.email)
        password.setText(mySharedHelper.pass)
        loginPresenter = LoginPresenter(this)
        login.setOnClickListener {
            loginPresenter.onLogin(email.text.toString(), password.text.toString())
            if(soundID!=0)
                sp?.play(soundID, .99f, .99f, 1, 0, .99f)
            //Mengecek jika checkbox "Remember Me" di ceklis
            if (remember.isChecked){
                //Maka simpan data email dan password
                mySharedHelper.email = email.text.toString()
                mySharedHelper.pass = password.text.toString()
            }
            else if (!remember.isChecked) {
                //Jika tidak, maka hapus semua data
                mySharedHelper.clearValues()
            }
        }
    }

    //Membuat fungsi untuk load soundPool saat activity dijalankan
    override fun onStart() {
        super.onStart()
        //Mengecek versi android yang lebih tinggi sama dengan LOLLIPOP
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
            //Jika ya, jalankan fungsi newSoundPool
            createNewSoundPool()
        else
            //Jika tidak, jalankan fungsi oldSoundPool
            createOldSoundPool()
        //Mengecek apakah soundPool berhasil di load
        sp?.setOnLoadCompleteListener{soundPool, id, status ->
            if(status!=0)
                Toast.makeText(this, "Gagal Load", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "Load Berhasil", Toast.LENGTH_SHORT).show()

        }
        //Menentukan file sound yang akan di load
        soundID = sp?.load(this, R.raw.welcome, 1)?:0
    }

    //Fungsi oldSoundPool
    private fun createOldSoundPool() {
        sp = SoundPool(15, AudioManager.STREAM_MUSIC, 0)

    }
    //Fungsi newSoundPool
    private fun createNewSoundPool() {
        sp = SoundPool.Builder().setMaxStreams(15).build()
    }

    //Membuat fungsi untuk menghentikan load soundPool
    override fun onStop() {
        super.onStop()
        sp?.release()
        sp = null
    }

}