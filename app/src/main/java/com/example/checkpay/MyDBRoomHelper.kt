package com.example.checkpay

import androidx.room.Database
import androidx.room.Entity
import androidx.room.RoomDatabase

//Membuat class untuk menuliskan entitas yang akan dibentuk pada database dan versi dari database
@Database(entities = arrayOf(Siswa::class),version = 1)
//MyDBRoomHelper berupa abstract class dan mengimplementasikan API RoomDatabase
abstract class MyDBRoomHelper : RoomDatabase(){
    //fungsi abstract yang mengirimkan DAO yang akan digunakan
    abstract fun SiswaDAO() : SiswaDAO
}