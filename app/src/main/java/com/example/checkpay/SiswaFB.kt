package com.example.checkpay

data class SiswaFB(
        val NIS: String,
        var nama: String = "",
        var kelas: String = "",
        var uangsklh: String = "",
        var ket: String = ""
)