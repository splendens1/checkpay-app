package com.example.checkpay

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import org.apache.http.conn.ssl.StrictHostnameVerifier

const val EXTRA_PESAN: String = "EXTRA_PESAN"

class MyAlarmReceiver : BroadcastReceiver() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context, intent: Intent) {
        //mendeklarasikan id untuk notifikasi
        val Notifyid = 30103
        //mendeklarasikan id dari channel notifikasi
        val Channel_id = "my_channel_01"
        //mendeklarasikan nama notifikasi
        val name = "ON/OFF"
        //mendeklarasikan tingkat kepentingan dari notifikasi
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        //mendeklarasikan Notification Channel
        val nNotifyChannel = NotificationChannel(Channel_id, name, importance)
        //membuat notifikasi
        val mBuilder = NotificationCompat.Builder(context!!, Channel_id)
                .setSmallIcon(R.drawable.logo)
                .setContentText(intent?.getStringExtra(EXTRA_PESAN))
                .setContentTitle("Alarm Manager")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        //mendeklarasikan notification manager
        var mNotificationManager = context
                .getSystemService(Context.NOTIFICATION_SERVICE)
                as NotificationManager
        //membuat perulangan pada notification manager
        for (s in mNotificationManager.notificationChannels){
            mNotificationManager.deleteNotificationChannel(s.id)
        }
        //mendaftarkan notifikasi pada notification manager
        mNotificationManager.createNotificationChannel(nNotifyChannel)
        //menampilkan notifikasi
        mNotificationManager.notify(Notifyid, mBuilder.build())
    }
}