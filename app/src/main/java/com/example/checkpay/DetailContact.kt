package com.example.checkpay

import android.annotation.SuppressLint
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_detail_contact.*
import kotlinx.android.synthetic.main.activity_konfirmasi.*

//Menambahkan implementasi LoaderManager.LoaderCallbacks<Cursor> yang wajib mengimplementasikan 3 metode :
//onCreateLoader(), onLoadFinished() dan onLoaderReset()
class DetailContact : AppCompatActivity(), LoaderManager.LoaderCallbacks<Cursor>,myAdapterRecyView.myOnClickListener{
    //menentukan variable yang akan digunakan untuk menentukan atribut yang perlu diambil pada CursorLoader
    var contactID = ContactsContract.Contacts._ID
    var DisplayName = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
    var Number = ContactsContract.CommonDataKinds.Phone.NUMBER
    var Email = ContactsContract.CommonDataKinds.Email.DATA
    var EmailUri = ContactsContract.CommonDataKinds.Email.CONTENT_URI
    var myListContact : MutableList<myContact> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_contact)
        //menyatakan dari mana context yang menjalankan loader
        LoaderManager.getInstance(this).initLoader(1,null,this)

    }
    //dijalankan ketika loader diinisialisasi menggunakan initloader pada aktivitas utama
    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        //menentukan uri data yang akan dibaca yaitu kontak
        var MyContactUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        //menentukan atribut kolom  yang akan dipilih
        var MyProjection = arrayOf(contactID, DisplayName, Number, Email)
        //mengembalikan CursorLoader yang berfungsi membaca data secara async dan diurutkan berdasarkan nomor telepon
        return CursorLoader(this,MyContactUri,MyProjection,null,null,"$Number ASC")
    }

    @RequiresApi(Build.VERSION_CODES.P)
    //dijalankan ketika pembacaan data selesai
    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor?) {
        myListContact.clear()
        if (data!=null){
            data.moveToFirst()
            //mengambil daftar kontak dengan contentResolver.query() berdasarkan argumen
            val c: Cursor? = contentResolver.query(EmailUri, null, null, null, "$Number ASC")
            val contacts = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null)
            while(!data.isAfterLast) {
                var email = "-"
                var bitmap : Bitmap? = null
                if (c != null && c.moveToNext()) {
                    //jika terdapat email maka ambil email dalam bentuk string dari hasil query berdasarkan nama atribut
                    email = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                }
                if (contacts != null && contacts.moveToNext()) {
                    //jika terdapat photo maka ambil uri photo
                    val photo_uri = contacts!!.getString(contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))
                    if(photo_uri!=null){
                        //ubah uri photo yang didapatkan menjadi bitmap
                        val source = ImageDecoder.createSource(contentResolver, Uri.parse(photo_uri))
                        bitmap = ImageDecoder.decodeBitmap(source)
                    }
                }
                //menambahkan ke arraylist nama, nomor, email dan photo
                myListContact.add(myContact(
                        data.getString(data.getColumnIndex(DisplayName)),data.getString(data.getColumnIndex(Number)),email,bitmap))
                data.moveToNext()
            }
            //menambahkan data ke adapter recycle view untuk ditampilkan
            var contactAdapter = myAdapterRecyView(myListContact,this@DetailContact)
            myRecyView.apply {
                layoutManager= LinearLayoutManager(this@DetailContact)
                adapter = contactAdapter
            }
        }
    }

    //dijalankan ketika pada loader terjadi reset
    override fun onLoaderReset(loader: Loader<Cursor>) {
        //memberitahukan kepada recycleview untuk mengupdate tampilan karena terjadi perubahan isi pada data
        myRecyView.adapter?.notifyDataSetChanged()
    }

    override fun onClickListener(position: Int) {
        var intent = Intent(this,activity_cekstatus::class.java)
        intent.putExtra(EXTRA_NO,myListContact[position].nomorHp)
        startActivity(intent)
    }
}