package com.example.checkpay

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


//Pembuatan class Parcelable
@Parcelize
data class Person(var NIS : String = "0", var Nama : String = "No Name", var Kelas : String = "Kosong", var Uang : Int = 0) : Parcelable{
}