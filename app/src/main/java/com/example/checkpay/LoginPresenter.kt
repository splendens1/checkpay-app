package com.example.checkpay

class LoginPresenter(internal var iLoginView: ILoginView): ILoginPresenter {
override fun onLogin(email: String, password: String) {
        val user = User(email,password)
        val loginCode = user.isDataValid()
        if (loginCode==0)
            iLoginView.onLoginError("Email tidak boleh kosong")
        else if(loginCode==1)
            iLoginView.onLoginError("Email salah")
        else if(loginCode==2)
            iLoginView.onLoginError("Password harus lebih dari 6 karakter")
        else if(loginCode==3)
            iLoginView.onLoginError("Password salah")
        else
            iLoginView.onLoginSuccess("Login berhasil")
    }
}