package MyDB

import android.provider.BaseColumns

object userDB{
    class userTable: BaseColumns {
        companion object {
            val TABLE_MURID = "tbl_Murid"
            val COLUMN_ID: String = "ID"
            val COLUMN_NIS: String = "NIS"
            val COLUMN_NAMA: String = "Nama"
            val COLUMN_KELAS: String = "Kelas"
            val COLUMN_UANGSKLH: String = "Uang_Sekolah"
            val COLUMN_KET: String = "Keterangan"
        }
    }
}