package com.example.checkpay

import MyDB.userDB
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class myDBHelper(context: Context)  : SQLiteOpenHelper(
    context, DATABASE_NAME, null, DATABASE_VERSION
){
    companion object {
        private val DATABASE_NAME = "mysqlitedb.db"
        private val DATABASE_VERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        //Membuat table pada database
        var CREATE_USER_TABLE = ("CREATE TABLE ${userDB.userTable.TABLE_MURID} " +
                "(${userDB.userTable.COLUMN_ID} INTEGER PRIMARY KEY," +
                "${userDB.userTable.COLUMN_NIS} TEXT," +
                "${userDB.userTable.COLUMN_NAMA} TEXT," +
                "${userDB.userTable.COLUMN_KELAS} TEXT," +
                "${userDB.userTable.COLUMN_UANGSKLH} TEXT," +
                "${userDB.userTable.COLUMN_KET} TEXT)")
        //Mengeksekusi query database
        db?.execSQL(CREATE_USER_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //Menghapus database jika memiliki nama yang sama
        db?.execSQL(
            "DROP TABLE IF EXISTS " +
                    "${userDB.userTable.TABLE_MURID}"
        )
        onCreate(db)
    }
    //Memulai transaction
    val beginTransaction:()->Unit = {
        this.writableDatabase.beginTransaction()
        this.readableDatabase.beginTransaction()
    }
    //Transaction sukses
    val successTransaction:()->Unit = {
        this.writableDatabase.beginTransaction()
        this.readableDatabase.beginTransaction()
    }
    //Mengakhiri Transaction
    val endTransaction:()->Unit = {
        this.writableDatabase.beginTransaction()
        this.readableDatabase.beginTransaction()
    }
    //Fungsi untuk menambah data murid
    fun addUserT (murid: Murid): Long{
        //Memasukkan data pada database
        val sqlString = "INSERT INTO ${userDB.userTable.TABLE_MURID} " +
                "(${userDB.userTable.COLUMN_ID}" +
                ",${userDB.userTable.COLUMN_NIS}" +
                ",${userDB.userTable.COLUMN_NAMA}" +
                ",${userDB.userTable.COLUMN_KELAS}" +
                ",${userDB.userTable.COLUMN_UANGSKLH}" +
                ",${userDB.userTable.COLUMN_KET}) VALUES (?,?,?,?,?,?)"
        val myStatement = this.writableDatabase.compileStatement(sqlString)
        //Proses binding sesuai field yang telah ditentukan
        myStatement.bindString(2,murid.nis)
        myStatement.bindString(3,murid.nama)
        myStatement.bindString(4,murid.kelas)
        myStatement.bindString(5,murid.uangsklh)
        myStatement.bindString(6,murid.ket)
        val result = myStatement.executeInsert()
        myStatement.clearBindings()
        return result
    }
    //Fungsi untuk membaca/menampilkan data murid
    fun readUserT (nis: String) : String {
        val select = "SELECT * FROM ${userDB.userTable.TABLE_MURID} WHERE ${userDB.userTable.COLUMN_NIS} = ${nis}"
        var cursor : Cursor ?= null
        cursor = this.readableDatabase.rawQuery(select, null)
        var hasil = ""
        if (cursor.moveToFirst()){
            do {
                hasil = cursor.getString(cursor.getColumnIndex(userDB.userTable.COLUMN_KET))
            }while (cursor.moveToNext())
        }
        return hasil
    }
    //Fungsi untuk mengupdate data murid berdasarkan NIS
    fun updateUserT (ket: String, nis: String) : Int {
        val sqlString = "UPDATE ${userDB.userTable.TABLE_MURID} " +
                "SET ${userDB.userTable.COLUMN_KET} = ?" +
                "WHERE ${userDB.userTable.COLUMN_NIS} = ?"
        val myStatement = this.writableDatabase.compileStatement(sqlString)
        myStatement.bindString(1,ket)
        myStatement.bindString(2,nis)
        val result = myStatement.executeUpdateDelete()
        myStatement.clearBindings()
        return result
    }
    //Menghapus data murid
    fun deleteUser() {
        this.readableDatabase.delete(userDB.userTable.TABLE_MURID,null,null)
    }
}
