package com.example.checkpay

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.MediaController
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.OnUserEarnedRewardListener
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import kotlinx.android.synthetic.main.activity_media_player.*

class media_player : AppCompatActivity() {
    private var mRewardVid : RewardedAd? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media_player)

//        //membuat reward ads
//        RewardedAd.load(this,"ca-app-pub-3940256099942544/5224354917",
//                AdRequest.Builder().build(), object : RewardedAdLoadCallback(){
//            override fun onAdFailedToLoad(p0: LoadAdError) {
//                super.onAdFailedToLoad(p0)
//                Toast.makeText(this@media_player, "Load Faild", Toast.LENGTH_SHORT).show()
//                mRewardVid = null
//            }
//
//            override fun onAdLoaded(p0: RewardedAd) {
//                super.onAdLoaded(p0)
//                mRewardVid = p0
//                mRewardVid?.show(this@media_player, OnUserEarnedRewardListener{
//
//                })
//            }
//        })

        //Media Player
        val media = MediaController(this)
        media.setAnchorView(mediaplay)
        val videooff = Uri.parse("android.resource://$packageName/${R.raw.covid19}")
        mediaplay.setMediaController(media)
        mediaplay.setVideoURI(videooff)
        mediaplay.requestFocus()
        mediaplay.start()
    }
}